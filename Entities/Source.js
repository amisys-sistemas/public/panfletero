class Source {

    constructor(name, feed, profile, order, targets, content) {

        this.name = name;                   // Nome de exibição da fonte
        this.feed = feed;                   // URL de acesso ao feed RSS
        this.profile = profile;             // Recebe o profile definido no arquivo CSV
        this.order = order.toUpperCase();   // Ordem de disparo do conteudo (ver abaixo)
        this.targets = targets;             // Recebe os destinos das publicações
        this.content = content;             // Objetos de mídias carregadas para disparo

        this.lastLoad = new Date();         // Data e hora da última carga de conteúdo

        this.sortNews();                    // Aplica a ordenação de artigos
        this.profileApply();                // Aplica o perfil de configuração
        
    }

    sortNews(order = this.order) {

        if (!Array.isArray(this.content)) throw 'Lista de conteúdos inválida';

        /**
         *  FIFO: Ordem do download crescente;
         *  FILO: Ordem do download decrescente;
         *  LIFO: Data de publicação crescente;
         *  LILO: Data de publicação decrescente;
         *  ALEA: Embaralhado aleatoriamente;
         */

        switch (order.toUpperCase()) {
            case 'FIFO':
                this.content.reverse();
                break;
            case 'FILO':
                break;
            case 'LIFO':
                this.content.sort((a, b) => {
                    return a.isoDate > b.isoDate;
                })
                break;
            case 'LILO':
                this.content.sort((a, b) => {
                    return a.isoDate < b.isoDate;
                })
                break;
            case 'ALEA':
                for (let i = this.content.length - 1; i > 0; i--) {
                    let j = Math.floor(Math.random() * (i + 1));
                    [this.content[i], this.content[j]] = [this.content[j], this.content[i]];
                }
                break;
            default:
                throw 'Parâmetro de ordenamento inválido para "' + this.name + '": ' + order;
        }

    }

    profileApply() {

        switch (this.profile) {
            case 'default':
                // Nenhuma modificação é executada
                break;
            case 'primeirasDez':
                this.content.splice(10);
                break;
            default:
                throw 'Perfil inválido para "' + this.name + '".';
        }
    }

    getArticle() {

        let clear = (str) => {
            return str.replace(/\r?\n|\r/g, "").trim();
        }

        if (this.content.length == 0) throw 'Lista de artigos vazia em ' + this.name;
        
        let item = this.content.shift();

        let _d = new Date(item.isoDate || clear(item.pubDate));

        let article = {
            target:     '',
            head:       `📰 ${this.name.toUpperCase()} | "${clear(item.title)}"`,
            date:       `🗓️ ${_d.getDate()}/${_d.getMonth()+1}/${_d.getFullYear()}`,
            peek:       `🔎 "${item.contentSnippet}`,
            link:       `🔗 ${clear(item.link)}`,
            foot:       '📣 #panfleterobot'
        }

        let list = [];

        for (let target of this.targets) {

            switch(target) {
                case 'mastodon':
                    list.push({
                        target: 'mastodon',
                        params: {
                            spoiler_text:   article.head,
                            status:         `${article.peek.substring(0,149)}"...`
                                          + `\r\n`
                                          + article.date
                                          + `\r\n\r\n`
                                          + article.link
                                          + `\r\n\r\n`
                                          + article.foot + ' #news #bot',
                            visibility:     'unlisted'
                        }
                    });
                    break;
                case 'twitter':
                    list.push({
                        target: 'twitter',
                        params: {
                            status:     article.head
                                      + `\r\n`
                                      + article.date
                                      + `\r\n\r\n`
                                      + article.link
                                      + `\r\n\r\n`
                                      + article.foot,
                        }
                    });
                    break;
                case 'telegram':
                    // Implementar
                    break;
                default:
                    throw 'Alvo de publicação inválido para "' + this.name + '".';
            }

        }        

        return list;

    }

}

module.exports = Source;