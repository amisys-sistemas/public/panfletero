const rss = require('rss-parser');
const sources = require('../sources.json');

module.exports = {

    load() {

        return sources.list;
    },

    async get(feed) {
       
        return new Promise((resolve,reject) => {

                let reader = new rss({
                    headers: {
                        'User-Agent': 'Panfletero RSS Bot v0.9 beta',
                        'Accept': 'text/html,application/xhtml+xml,application/xml'
                    },
                    // defaultRSS: 2,
                    timeout: 10000
                });

                reader.parseURL(feed, (error,data) => {
                    if (error) reject(error);
                    resolve(data);
                })
        })
    }

}